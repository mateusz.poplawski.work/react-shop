import React, { useCallback, useContext, useEffect } from 'react';

import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar/AppBar';
import Button from '@material-ui/core/Button/Button';
import IconButton from '@material-ui/core/IconButton/IconButton';
import Toolbar from '@material-ui/core/Toolbar/Toolbar';
import Typography from '@material-ui/core/Typography/Typography';
import MenuIcon from '@material-ui/icons/Menu';
import { UserMenu } from './UserMenu';
import { useHistory, Link } from 'react-router-dom';
import { AuthContext } from 'services/AuthContext';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            flexGrow: 1,
        },
        menuButton: {
            marginRight: theme.spacing(2),
        },
        title: {
            flexGrow: 1,
        },
        link: {
            color: 'inherit',
            textDecoration: 'none'
        }
    }),
);

export function TopBar() {
    const classes = useStyles();
    const history = useHistory();
    const { user, setUser } = useContext(AuthContext);

    useEffect(() => {
        setTimeout(() => {
            console.log('set user to null')
            setUser(null);
        }, 300)
    }, [user, setUser]);

    const onLoginButtonClick = useCallback(() => {
        history.push('/login');
        console.log('Login button click!');
    }, [history]);

    const handleMenuClick = () => { };
    return (
        <AppBar position="static">
            <Toolbar>
                <IconButton edge="start" className={classes.menuButton} color="inherit" aria-label="menu">
                    <MenuIcon />
                </IconButton>
                <Typography variant="h6" className={classes.title}>
                    <Link className={classes.link} to="/">
                        Tutorial Shop
                    </Link>
                </Typography>
                {
                    user === null &&
                    <Button onClick={onLoginButtonClick} color="inherit">
                        Login
                </Button>
                }
                {user !== null && <UserMenu handleMenuClick={handleMenuClick} />}

            </Toolbar>
        </AppBar>
    );
}
