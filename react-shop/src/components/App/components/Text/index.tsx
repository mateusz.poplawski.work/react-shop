import React, { MouseEvent, useCallback } from 'react';

interface TextProps {
    readonly onLinkCliked: () => void;
}

export function Text({ onLinkCliked }: TextProps) {
    const onClick = useCallback(
        (e: MouseEvent<HTMLAnchorElement>) => {
            e.preventDefault();
            onLinkCliked();
        },
        [onLinkCliked],
    );

    return (
        <React.Fragment>
            <p>
                Edit <code>src/App.tsx</code> and save to reload
            </p>
            <a onClick={onClick} className="App-link" href="www.google.com" target="_blank" rel="noopener noreferrer">
                Learn React
            </a>
        </React.Fragment>
    );
}
