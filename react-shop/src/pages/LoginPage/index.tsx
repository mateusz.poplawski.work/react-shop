import { Container, makeStyles, Paper, Typography } from '@material-ui/core';
import React, { useCallback, useContext } from 'react';
import { Redirect } from 'react-router-dom';
import GoogleButton from 'react-google-button';
import { AuthContext } from 'services/AuthContext';

const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(8),
        padding: theme.spacing(2),
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    loginButton: {
        marginTop: theme.spacing(2),
    },
}));

export function LoginPage() {
    const classes = useStyles();
    const { user } = useContext(AuthContext);

    const onLoginButtonClick = useCallback(() => {
        console.log('button click');
    }, []);

    if (user !== null) {
        return <Redirect to="/" />;
    }
   
    return (
        <Container maxWidth="sm">
            <Paper className={classes.paper}>
                <Typography component="h1" variant="h5">
                    Log in
                </Typography>
                <GoogleButton onClick={onLoginButtonClick} className={classes.loginButton}>LOGIN BUTTON</GoogleButton>
            </Paper>
        </Container>
    );
}
